package com.example.gareth1305.prasbic_android.entity;

/**
 * Created by Gareth1305 on 20/11/2015.
 */
public class User {
    private int id;
    private String email;
    private String firstName;
    private String lastName;
    private String password;
    private String emergencyContactNum;

    public User(int id, String email, String firstName,
                String lastName, String password, String emergencyContactNum) {
        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.emergencyContactNum = emergencyContactNum;
    }

    public User(String email, String firstName,
                String lastName, String password, String emergencyContactNum) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.emergencyContactNum = emergencyContactNum;
    }

    public User() { }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmergencyContactNum() {
        return emergencyContactNum;
    }

    public void setEmergencyContactNum(String emergencyContactNum) {
        this.emergencyContactNum = emergencyContactNum;
    }

    @Override
    public String toString() {
        return "User{" +
                "email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", password='" + password + '\'' +
                ", emergencyContactNum='" + emergencyContactNum + '\'' +
                '}';
    }
}
