**Project Name** - Prasbic - Preventing anti-social behavior in communities

**Description** - The project was designed to allow users to report sightings of anti-social behavior in their communities. It also includes a 'panic' button which will allow a user to send a text to their emergency contact containing their current location. The project is an android application which allows users to: 
* Sign in 
* Register
* Report anti-social behavior in their communities
* Send a text with their location to their emergency contact on:
      - the click of a button
      - when the user shakes the phone (using sensors)
* View reports which are close to a their location via Google Maps (Geo Coding)
* View detailed reports by using a Custom List View with a List Adapter
* Pre-populate form fields based on their location (Reverse Geo Coding)
* Post/Receive data from a RESTful web service using HttpRequest/HttpResponse
* Use gestures to move between pages

This project uses RESTful web services and JPA from the [attached project](https://bitbucket.org/Gareth1305/prasbic_android_assignment_spring). 

This project is ongoing and more functionality will be added soon. This project focuses primarily on back end functionality. Some images of the app are included


![HomePageActivity.jpg](https://bitbucket.org/repo/o4Kd5k/images/1204431042-HomePageActivity.jpg)
![RegisterActivity.jpg](https://bitbucket.org/repo/o4Kd5k/images/743817194-RegisterActivity.jpg)
![SubmitASBOReportActivity.jpg](https://bitbucket.org/repo/o4Kd5k/images/4069894991-SubmitASBOReportActivity.jpg)
![ViewReportsOnMapActivity.jpg](https://bitbucket.org/repo/o4Kd5k/images/524601025-ViewReportsOnMapActivity.jpg)
![DetailedListViewActivity.jpg](https://bitbucket.org/repo/o4Kd5k/images/1359155633-DetailedListViewActivity.jpg)
![EmergencyTextActivity.jpg](https://bitbucket.org/repo/o4Kd5k/images/3819984820-EmergencyTextActivity.jpg)